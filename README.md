# Inkscape extension removeDuplicateLines

An Inkscape extension to remove duplicate line segments with an option to add a tolerance for regarding the segments as a match.

The extension will appear in Extensions -> Cutlings

Read more and find examples [here](http://cutlings.wasbo.net/inkscape-extension-removeduplicatelines/).
